/**
 * The measuring functionality for other api
 * @type {request}
 */
var request = require('request');

var formData = {
    app_key: '5240f691-60b0-4360-ac1f-601117c5408f',
    phone: '+992901000535',
    name: 'Muhammad'
};
var remoteUrl = 'https://core.codepr.ru/api/v2/crm/user_create_or_update'

var start_time = new Date().getTime();

request.post(
    {
        url: remoteUrl,
        form: formData,
    },
    function (err, httpResponse, body) {

        console.log('Time elapsed since queuing the request:', new Date().getTime() - start_time);
    }
);

request.post(
    {
        url: remoteUrl,
        form: formData,
        time: true
    },
    function (err, httpResponse, body) {

        console.log('The actual time elapsed:', httpResponse.elapsedTime);
    }
);


