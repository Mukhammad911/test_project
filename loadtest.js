/*
For load testing remote api address
*/

const loadtest = require('loadtest');

const options = {
    url: 'https://core.codepr.ru/api/v2/crm/user_create_or_update',
    concurrency: 10, // loadtest will create a certain number of clients; this parameter controls how many. Requests from them will arrive concurrently to the server.
    maxRequests: 10, // Note: the total number of requests sent can be bigger than the parameter if there is a concurrency parameter; loadtest will report just the first
    method:'POST',
    contentType:'application/json',
    body:{
        app_key: '5240f691-60b0-4360-ac1f-601117c5408f',
        phone: '+992901000535',
        name: 'Muhammad'
    }
};

loadtest.loadTest(options, function(error, result)
{
    if (error)
    {
        return console.error('Got an error: %s', error);
    }
    console.log('Tests run successfully');

    console.log(result);
});